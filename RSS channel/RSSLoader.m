//
//  RSSLoader.m
//  RSS channel
//
//  Created by Капитан on 12.07.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import "RSSLoader.h"

@implementation RSSLoader {
    NSMutableDictionary *item;
    NSMutableString     *currentTitle;
    NSMutableString     *currentDate;
    NSMutableString     *currentSummary;
    NSMutableString     *currentLink;
    NSString            *currentElement;
    NSURL               *imageURL;
    UIImage             *image;
}

- (void)fetchRSSWithURL:(NSURL*)url {
    NSURLResponse *response = nil;
    NSError *error = nil;

    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSData *data = [NSURLConnection sendSynchronousRequest:request
                                           returningResponse:&response
                                                       error:&error];
    _stories = [NSMutableArray new];
    _xmlParser = [[NSXMLParser alloc] initWithData:data];
    _xmlParser.delegate = self;
    [_xmlParser setShouldProcessNamespaces: NO];
    [_xmlParser setShouldReportNamespacePrefixes: NO];
    [_xmlParser setShouldResolveExternalEntities: NO];    
    if ([self.xmlParser parse]){
        NSLog(@"The XML is parsed.");
    } else {
        NSLog(@"Failed to parse the XML");
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }
}

#pragma mark - NSXMLParser parsing

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
	NSString * errorString = [NSString stringWithFormat:@"Unable to download story feed from web site (Error code %li )", (long)[parseError code]];
	NSLog(@"error parsing XML: %@", errorString);
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	UIAlertView * errorAlert = [[UIAlertView alloc] initWithTitle:@"Error loading content" message:errorString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[errorAlert show];
}

- (void)parserDidStartDocument:(NSXMLParser *)parser {
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName
                                         namespaceURI:(NSString *)namespaceURI
                                        qualifiedName:(NSString *)qName
                                           attributes:(NSDictionary *)attributeDict {
    currentElement = [elementName copy];
    if ([elementName isEqualToString:@"item"]) {
		item           = [NSMutableDictionary new];
		currentTitle   = [NSMutableString new];
		currentDate    = [NSMutableString new];
		currentSummary = [NSMutableString new];
		currentLink    = [NSMutableString new];
        image          = [UIImage new];
	} if ([elementName isEqualToString:@"enclosure"]){
        NSString *imageStr = [NSString stringWithString:[attributeDict objectForKey:@"url"]];
        imageURL = [NSURL URLWithString:imageStr];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    if ([currentElement isEqualToString:@"title"]) {
		[currentTitle appendString:string];
	} else if ([currentElement isEqualToString:@"link"]) {
		[currentLink appendString:string];
	} else if ([currentElement isEqualToString:@"description"]) {
		[currentSummary appendString:string];
	} else if ([currentElement isEqualToString:@"pubDate"]) {
		[currentDate appendString:string];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName
                                       namespaceURI:(NSString *)namespaceURI
                                      qualifiedName:(NSString *)qName {
    if ([elementName isEqualToString:@"item"]) {
		[item setObject:currentTitle forKey:@"title"];
		[item setObject:currentLink forKey:@"link"];
		[item setObject:currentSummary forKey:@"summary"];
		[item setObject:currentDate forKey:@"date"];
        if (imageURL != nil) {
            [item setObject:imageURL forKey:@"url"];
            image = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageURL]];
            UIImage *thumbnail = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageURL]];
            if (thumbnail == nil) {
                thumbnail = [UIImage imageNamed:@"noimage.png"] ;
            }
            CGSize itemSize = CGSizeMake(90, 60);
            UIGraphicsBeginImageContext(itemSize);
            CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height);
            [thumbnail drawInRect:imageRect];
            image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [item setObject:image forKey:@"image"];
        }
		[_stories addObject:[item copy]];
    }
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    NSLog(@"arraynews %lu", (unsigned long)[_stories count]);
}

@end
