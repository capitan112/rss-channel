//
//  DetailViewController.m
//  RSS channel
//
//  Created by Капитан on 12.07.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation DetailViewController

#pragma mark - Managing the detail item

- (void)viewDidLoad {
    [super viewDidLoad];
    _webView.delegate = self;
    [self loadPage:_urlString];
}

- (void)viewWillAppear:(BOOL)animated{

}

- (void)loadPage:(NSURL*)urlString {
    NSURLRequest *request = [NSURLRequest requestWithURL:urlString];
    [_webView loadRequest:request];
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
