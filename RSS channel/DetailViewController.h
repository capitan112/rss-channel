//
//  DetailViewController.h
//  RSS channel
//
//  Created by Капитан on 12.07.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) NSURL* urlString;

@end
