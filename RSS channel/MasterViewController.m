//
//  MasterViewController.m
//  RSS channel
//
//  Created by Капитан on 12.07.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

static int offset = 10;
static int currentRow = 0;
const CGFloat tableViewCellHeight = 80;

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "RSSLoader.h"

@interface MasterViewController ()

@property (nonatomic, strong) RSSLoader      *rssLoader;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic, strong) DetailViewController *detailViewConroller;
@property (nonatomic,retain) UITableView *tableView;

@end

@implementation MasterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadRSSSource) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    _rssLoader = [RSSLoader new];
    [self RSSloader];
}

#pragma mark RSS loader

- (void)RSSloader {
    currentRow = offset;
    if ([_rssLoader.stories count] == 0) {
//        NSURL *feedURL = [NSURL URLWithString:@"http://news.liga.net/all/rss.xml"];
        NSURL *feedURL = [NSURL URLWithString:@"http://rss.cnn.com/rss/edition_football.rss"];
//        NSURL *feedURL = [NSURL URLWithString:@"http://www.cnbc.com/id/100003114/device/rss/rss.html"];
        [_rssLoader fetchRSSWithURL:feedURL];
	}
}

#pragma mark refresh RSS data

- (void)reloadRSSSource {
    [self RSSloader];
    [self.tableView reloadData];
    [self.refreshControl endRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return currentRow;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *postCellId = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:postCellId ];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:postCellId ];
    }
    cell.textLabel.text = [[_rssLoader.stories objectAtIndex: indexPath.row] objectForKey: @"title"];
    cell.textLabel.numberOfLines = 3;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.detailTextLabel.text = [[_rssLoader.stories objectAtIndex: indexPath.row] objectForKey: @"summary"];
    cell.detailTextLabel.numberOfLines = 2;
    cell.imageView.image = [[_rssLoader.stories objectAtIndex: indexPath.row] objectForKey: @"image"];

    return cell;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    float endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height;
    if (endScrolling >= scrollView.contentSize.height) {
        int leaveRows = (int)[_rssLoader.stories count] - currentRow;
        if (leaveRows > 0 && leaveRows > offset){
            currentRow += offset;
        } else if (leaveRows > 0 && leaveRows < offset) {
            currentRow += leaveRows;
        }
        [self.tableView reloadData];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString * storyLink = [[_rssLoader.stories objectAtIndex: indexPath.row] objectForKey: @"link"];
    storyLink = [storyLink stringByReplacingOccurrencesOfString:@" " withString:@""];
    storyLink = [storyLink stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    storyLink = [storyLink stringByReplacingOccurrencesOfString:@"	" withString:@""];
    _detailViewConroller.urlString = [NSURL URLWithString:storyLink];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
        return tableViewCellHeight;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        _detailViewConroller = segue.destinationViewController;
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        _detailViewConroller.urlString = [[_rssLoader.stories objectAtIndex:indexPath.row] objectForKey: @"url"];
    }
}

@end
